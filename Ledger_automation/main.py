import sys
from lib import *
from main_settings import *
        
def main(argv):
    #read configurations from arguments
    configs = options(argv, SCRIPT_OPTIONS, SCRIPT_DESC, SCRIPT_EXM)

    #overwrites (for test)
    configs['input_file'] = 'statement.csv'
    configs['config_file'] = 'config_SB_LV.json'
    configs['script_action'] = 'edit_file.ledger'

    #process .json conf file and receive list of categories with examples
    cat_list = config_processing(load_json(configs['config_file']))

    #process input file
    trans_list = trans_processing(load_csv(configs['input_file']))

    #check if user wants to calculate commision fee separately
    total_commision_fee = 0
    if configs['commision_fee_calc']=='True':
        for rec in trans_list:
            rec.amount, total_commision_fee = calc_comfee(rec.details, rec.amount, total_commision_fee)

    #match transactions with appropriate category from config file
    find_category(cat_list, trans_list)
        
    #create dictionary of emacs records (bundled by date)
    emacs_dict = sort_trans(trans_list)
    
    #create ledger file
    credit_account = "Assets:Checking:Swedbank LV"
    create_ledger(
        script_action = configs['script_action'],
        emacs_dict = emacs_dict,
        credit_account = credit_account,
        total_commision_fee = total_commision_fee,
        bkp = configs['make_backup'],
        bkp_path = configs['backup_path']
    )
    
if __name__ == "__main__":
    main(sys.argv[1:])
    print('----script done-----')
