from lib import Option

#set script options; for example 'python main.py -i my_file.txt -c conf_file.txt'
SCRIPT_OPTIONS = [
    Option(fullname="input_file", letter="i", example="filename", desc="Input file", required=True),
    Option(fullname="config_file", letter="c", example="filename.json", desc="Configuration file", required=True),
    Option(fullname="make_backup", letter="b", example="True/False", desc="Backup for ledger-file; Default 'true'", default=True),
    Option(fullname="backup_path", letter="p", example="'path'", desc="Path where to save backups; Default './backups'", default='./backups'),
    Option(fullname="script_action", letter="e", example="new/filename", desc="New or edit ledger file; Default 'new'", default='new'),
    Option(fullname="commision_fee_calc", letter="q", example="True/False", desc="Separate commision fee calc; Default 'True'", default='True')
]

#script description and usage example - used in help 'pyton main.py -h'
SCRIPT_DESC = "Script to process bank statement into ledger-mode emacs file."
SCRIPT_EXM = "Example: python main.py -i statement.xls -c swedbank_lv.json -b false -p './my_backups' -e 'finances.ledger'"
