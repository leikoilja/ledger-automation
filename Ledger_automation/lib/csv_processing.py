import csv
import os
import re
from shutil import copyfile
from datetime import datetime
from itertools import ifilterfalse

class Transaction:
    def __init__(self, **kwarg):
        self.date = date_conv(kwarg['date'])
        self.payer = kwarg['payer']
        self.details = kwarg['details']
        self.currency = kwarg['currency']
        self.amount = kwarg['amount']
        if "category" in kwarg:
            self.category = kwarg['category']
        else:
            self.category = "Unknown"

        #convert amount to float
        if not isinstance(self.amount, float) and self.amount!="Amount":
            self.amount = float(self.amount.replace(",","."))
   
    def print_h(self):
        print("Date - %s"%self.date)
        print("Payer - %s"%self.payer)
        print("Details - %s"%self.details)
        print("Amount - %s"%self.amount)
        print("Currency - %s"%self.currency)
        print("Category - %s"%self.category)

    def set_category(self, tmp):
        self.category = tmp

def date_conv(date_str):
    #check if date_str matches format '12.08.2018'
    try:
        return datetime.strptime(date_str, '%d.%m.%Y').strftime('%Y/%m/%d')
    except ValueError:
        return date_str

def calc_comfee(details, amount, total_commision_fee):
    if "konvertcijas maksa" in details.lower():
        s_start = 'konvertcijas maksa '.lower()
        s_end = 'EUR'.lower()
        s = details.lower()
        commision_fee = float(s[s.find(s_start)+len(s_start):s.rfind(s_end)])
        amount-=commision_fee
        total_commision_fee+=commision_fee
    return amount, total_commision_fee
        
def load_csv(filename):
    with open(filename, 'r') as f:
        reader = csv.reader(f, delimiter=';')
        tmp_list = list(reader)
    return tmp_list

def removeNonAscii(s):
    return "".join(i for i in s if ord(i) < 128)

def trans_processing(tmp_list):
    trans_list = []
    
    for rec in tmp_list:
        trans_list.append(
            Transaction(date=rec[2], payer=removeNonAscii(rec[3]), details=removeNonAscii(rec[4]), amount=rec[5], currency=rec[6]))

    #remove column's name line
    if trans_list[0].details == "Details" and trans_list[0].date == "Date":
        trans_list.pop(0) #remove column name line
    #remove Opening balance line
    if trans_list[0].details == "Opening balance":
        trans_list.pop(0)
    #remove Closing balance line
    if trans_list[-1].details == "Closing balance":
        trans_list.pop(-1)
    #remove Debit turnover line
    if trans_list[-1].details == "Turnover":
        trans_list.pop(-1)
    return trans_list
            
    
def find_category(cat_list, trans_list):
    #loop through trascactions
    for trans in trans_list:
        cat_found = False
        for cat in cat_list:
            for rec in cat.examples:
                if rec.lower() in trans.payer.lower():
                    #transaction matching category
                    trans.set_category(cat.name)
                    cat_found = True
        if not cat_found:
            #if category for transaction not found
            #offer to add payer to Category.examples (save to json at the end)
            #print("cat not found for %s"%trans.payer)
            pass
                    
def sort_trans(trans_list):
    dates = []
    tmp_dict_grp = {}
    #group transaction by dates
    for tran in trans_list:
        if tran.date not in dates:
            #if a new date is found - find all matching transactions
            dates.append(tran.date)
            tmp_list = []
            for rec in trans_list:
                if rec.date == tran.date:
                    tmp_list.append(rec)

            #bundle transactions with similar categories
            tmp_list_sorted = []
            cat_list = []
            for rec in tmp_list:
                if rec.category not in cat_list:
                    cat_list.append(rec.category)
                    tmp_amount = 0
                    for elm in tmp_list:
                        if elm.category == rec.category:
                            tmp_amount+=elm.amount
                    tmp_list_sorted.append(Transaction(
                        category=rec.category,
                        date=rec.date,
                        currency=rec.currency,
                        amount=tmp_amount,
                        details="Summation",
                        payer="Summation")
                    )
            tmp_dict_grp[tran.date]=tmp_list_sorted
    return tmp_dict_grp

def create_ledger(**kwarg):
    script_action = kwarg['script_action']
    emacs_dict = kwarg['emacs_dict']
    credit_account = kwarg['credit_account']
    total_commision_fee = kwarg['total_commision_fee']
    bkp = kwarg['bkp']
    bkp_path = kwarg['bkp_path']
    
    rec_spacing = "\t \t \t"
    rec_space = "\t"

    # if user wants to print commission fee separately - add it to emacs_dict as an element
    if total_commision_fee:
        emacs_dict[emacs_dict.keys()[-1]].append(Transaction(
            category="Expenses:Bank commission fee",
            date=emacs_dict.keys()[-1],
            currency="EUR",
            amount=str(total_commision_fee),
            details="Bank commission fee",
            payer="User")
        )

    #if user wants to create new file
    if script_action == "new":
        #create a new dir for output files
        ledger_path = './ledger_files'
        if not os.path.exists(ledger_path):
            os.makedirs(ledger_path)
        file_att = 'w+'
        file_to_edit = ledger_path+"/"+"test.ledger"
    elif script_action != "new":
        file_att = 'a'
        file_to_edit = script_action

        #create backup of the ledger file to edit
        if not os.path.exists(bkp_path):
            os.makedirs(bkp_path)
        tmp_name = file_to_edit.split('.')
        copyfile(file_to_edit,bkp_path+"/"+tmp_name[0]+"_"+datetime.now().strftime("%H_%M_%S__%d-%m-%Y")+"."+tmp_name[1])

        #read existing ledger file
        ledger_dict = read_ledger(file_to_edit)

        #if similar record already exists in the ledger file - pop the emacs_dict[record]
        for rec in emacs_dict.keys():
            if rec in ledger_dict:
                for line in ledger_dict[rec]:
                    emacs_dict[rec] = filter(lambda x: find_match(x, line), emacs_dict[rec])

        #clear empty records
        for rec in emacs_dict.keys():
            if len(emacs_dict[rec]) == 0:
                del emacs_dict[rec]

    #write to the file
    if len(emacs_dict):
        with open(file_to_edit, file_att) as f:
            if file_att=='a':
                f.write("\n\n")
            for rec in sorted(emacs_dict.iterkeys()):
                f.write(rec)
                f.write(" csv-generated\n")
                for elm in emacs_dict[rec]:
                    f.write(rec_space+elm.category)
                    f.write(rec_spacing)
                    f.write(elm.currency)
                    f.write(str(elm.amount))
                    f.write("\n")
                f.write(rec_space+credit_account)
                f.write("\n\n")

def find_match(list_elm, line):
    if ((list_elm.category in line) and (list_elm.currency + str(list_elm.amount)) in line):
        return False
    else:
        return True

def read_ledger(filename):
    with open(filename) as f:
        content = f.readlines()

    #find line indexes for all mentioned dates in the file
    indexes = []
    index = 0
    for line in content:
        if(re.match(r"201[0-9]/+", line)):
            indexes.append(index)
        index+=1

    #add last line
    indexes.append(index)

    ledger_dict = {}
    #walk by indexes and create dict with transactions of each date
    for index in indexes[:-1]:
        date = content[index].split(" ")[0]
        if date not in ledger_dict:
            ledger_dict[date] = []

        #find all transactions for date
        all_trans = content[index+1:indexes[indexes.index(index)+1]]
        for trans in all_trans:
            ledger_dict[date].append(trans)
    return ledger_dict
