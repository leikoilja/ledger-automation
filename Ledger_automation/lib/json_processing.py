import json

class Category:
    def __init__(self, **kwarg):
        self.name = kwarg['name']
        self.examples = kwarg['examples']

    def print_h(self):
        print("Name - %s"%self.name)
        print("Examples - %s"%self.examples)
        
def config_processing(ct):            
    tmp_list = []
    for rec in ct:
        tmp_list.append(Category(name=rec, examples=ct[rec]))
    return tmp_list

def load_json(filepath):
    try:
        return json.load(open(filepath))
    except Exception, e:
        print("Error reading json file: %s", str(e))       
    return None
