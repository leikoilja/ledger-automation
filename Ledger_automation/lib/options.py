import sys, getopt 

help_msg = 'To get help how to use the script run: main.py -h'

class Option:
    def __init__(self, **kwargs):
        self.fullname = kwargs['fullname']
        self.letter = kwargs['letter']
        self.arg = kwargs['example']
        self.desc = kwargs['desc']
        if 'required' in kwargs and kwargs['required']:
            self.required = True
        else:
            self.required = False

        if 'default' in kwargs:
            self.default = kwargs['default']


    def display(self):
        if not self.required:
            print("-%s \t %s \t %s"%(self.letter, self.arg, self.desc))
        else:
            print("-%s \t %s \t %s \t \t [mandatory argument]"%(self.letter, self.arg, self.desc))


def options(argv, SCRIPT_OPTIONS, SCRIPT_DESC, SCRIPT_EXM):
    #check if there are any mandatory parameters
    tmp_required = False
    for p_option in SCRIPT_OPTIONS:
        if p_option.required:
            tmp_required = True
            break

    #no arg & no required - pass
    #no arg & require - error
    #arg - process
    if not argv and not tmp_required:
        return []
    elif not argv and tmp_required:
        print("Missing mandatory arguments!")
        print (help_msg)
        sys.exit()
    elif argv:    
        try:
            tmp_options = "h"
            for p_option in SCRIPT_OPTIONS:
                tmp_options+=(p_option.letter+":")
            opts, arg = getopt.getopt(argv, tmp_options)
        except getopt.GetoptError:
            print ('Parameters input error. Use -h for help.')
            sys.exit(2)

        configs = {}    
        for opt, arg in opts:
            if opt == "-h":
                print(SCRIPT_DESC)
                print('Use such as:')
                print("-option \t 'argument' \t description \n")
                for p_option in SCRIPT_OPTIONS:
                    p_option.display()
                print("\n"+SCRIPT_EXM)
                sys.exit()
            else:
                for p_option in SCRIPT_OPTIONS:
                    if opt.split('-')[1] == p_option.letter:
                        configs[p_option.fullname]=arg

        #min required arguments        
        tmp_missing = False
        for p_option in SCRIPT_OPTIONS:
            if (p_option.required and p_option.fullname not in configs):
                print('Mandatory argument missing - %s'%p_option.desc)
                tmp_missing = True
        if tmp_missing:
            print(help_msg)
            sys.exit()

        #assign default value, if specified
        for p_option in SCRIPT_OPTIONS:
            if p_option.fullname not in configs and p_option.default:
                configs[p_option.fullname]=p_option.default
        return(configs)
